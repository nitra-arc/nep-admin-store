<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomUrlType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('url', 'text', array(
            'label'     => ' ',
            'required'  => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'custom_url';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NitraStoreBundle',
            'data_class'         => 'Nitra\\StoreBundle\\Document\\Embedded\\CustomUrl',
        ));
    }
}