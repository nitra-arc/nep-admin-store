<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

class ProductManagementType extends AbstractType
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \Symfony\Component\HttpKernel\KernelInterface
     */
    protected $kernel;

    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $translator;

    /**
     * @var array Дополнительные поля
     */
    protected $additionalProductFields = array(
        'description',
    );

    public function __construct(ContainerInterface $container)
    {
        $this->container  = $container;
        $this->kernel     = $container->get('kernel');
        $this->translator = $container->get('translator');
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('ignoreCopyFields', 'nl_choice_double_list', array(
            'label'   => 'productManagement.ignoreCopyFields.label',
            'help'    => 'productManagement.ignoreCopyFields.help',
            'choices' => $this->getAllowedFields(array(
                'translations',
                'model',
            )),
        ));
        $builder->add('commonProductsFields', 'nl_choice_double_list', array(
            'label'   => 'productManagement.commonProductsFields.label',
            'help'    => 'productManagement.commonProductsFields.help',
            'choices' => $this->getAllowedFields(array(
                'translations',
                'model',
                'article',
                'parameters',
                'description',
                'seoInfo',
            )),
        ));
        $builder->add('fullProductName', 'nlcollection', array(
            'label'        => 'productManagement.fullProductName.label',
            'help'         => 'productManagement.fullProductName.help',
            'type'         => 'embed_product_management_product_names',
            'allow_add'    => true,
            'allow_delete' => true,
            'prototype'    => true,
            'callbacks'    => array(
                'pre_add' => 'newJavascript = newJavascript.replace(/__parentName__/g, \'fullProductName\');',
            ),
        ));
        $builder->add('shortProductName', 'nlcollection', array(
            'label'        => 'productManagement.shortProductName.label',
            'help'         => 'productManagement.shortProductName.help',
            'type'         => 'embed_product_management_product_names',
            'allow_add'    => true,
            'allow_delete' => true,
            'prototype'    => true,
            'callbacks'    => array(
                'pre_add' => 'newJavascript = newJavascript.replace(/__parentName__/g, \'shortProductName\');',
            ),
        ));
        $builder->add('fullModelName', 'nlcollection', array(
            'label'        => 'productManagement.fullModelName.label',
            'help'         => 'productManagement.fullModelName.help',
            'type'         => 'embed_product_management_product_names',
            'allow_add'    => true,
            'allow_delete' => true,
            'prototype'    => true,
            'options'      => array(
                'products'   => false,
            ),
            'callbacks'    => array(
                'pre_add' => 'newJavascript = newJavascript.replace(/__parentName__/g, \'fullModelName\');',
            ),
        ));
        $builder->add('shortModelName', 'nlcollection', array(
            'label'        => 'productManagement.shortModelName.label',
            'help'         => 'productManagement.shortModelName.help',
            'type'         => 'embed_product_management_product_names',
            'allow_add'    => true,
            'allow_delete' => true,
            'prototype'    => true,
            'options'      => array(
                'products'   => false,
            ),
            'callbacks'    => array(
                'pre_add' => 'newJavascript = newJavascript.replace(/__parentName__/g, \'shortModelName\');',
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'embed_product_management';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\\StoreBundle\\Document\\Embedded\\ProductManagement',
            'translation_domain' => 'NitraStoreBundle',
        ));
    }

    /**
     * Get allowed fields for products
     * @param array $ignoreFields
     * @return array
     */
    protected function getAllowedFields(array $ignoreFields)
    {
        $generator  = $this->getProductGeneratorPath();

        $parsed     = Yaml::parse($generator);

        $editFields = array_merge(
            $this->getEditFields($parsed),
            $this->additionalProductFields
        );

        return $this->formatAllowedFields($editFields, $parsed, $ignoreFields);
    }

    /**
     * Get path to Product-generator.yml
     * @return string Path to Product-generator.yml file
     */
    protected function getProductGeneratorPath()
    {
        $bundlePath = $this->kernel->getBundle('NitraProductBundle')->getPath();

        return $bundlePath .
            DIRECTORY_SEPARATOR .
            'Resources' .
            DIRECTORY_SEPARATOR .
            'config' .
            DIRECTORY_SEPARATOR .
            'Product-generator.yml'
        ;
    }

    /**
     * Collect edit fields from parsed generator
     * @param array $parsed
     * @return array
     */
    protected function getEditFields(array $parsed)
    {
        $result = array();

        foreach ($parsed['builders']['edit']['params']['tabs'] as $tab) {
            foreach ($tab as $fieldset) {
                $result = array_merge($result, $fieldset);
            }
        }

        return $result;
    }

    /**
     * Format (translate) allowed fields
     * @param array $fields
     * @param array $parsed
     * @param array $ignoreFields
     * @return array
     */
    protected function formatAllowedFields($fields, $parsed, $ignoreFields)
    {
        $result = array();

        foreach ($fields as $field) {
            if (in_array($field, $ignoreFields)) {
                continue;
            }
            $result[$field] = $this->translator->trans(
                $parsed['params']['fields'][$field]['label'],
                array(),
                'NitraProductBundleProduct'
            );
        }

        return $result;
    }
}