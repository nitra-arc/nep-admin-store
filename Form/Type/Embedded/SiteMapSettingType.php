<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SiteMapSettingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('categories', 'nl_tree_category', array(
            'label'    => 'siteMapSettings.categories',
            'required' => false,
            'multiple' => true,
        ));
        $builder->add('products', 'nl_accessories', array(
            'label'     => 'siteMapSettings.products.label',
            'required'  => false,
            'cat_class' => 'Nitra\\ProductBundle\\Document\\Category',
            'cat_label' => 'siteMapSettings.products.category',
            'multiple'  => true,
            'class'     => 'Nitra\\ProductBundle\\Document\\Product',
        ));
        $builder->add('informations', 'afe_double_list_document', array(
            'label'    => 'siteMapSettings.informations',
            'required' => false,
            'multiple' => true,
            'class'    => 'Nitra\\InformationBundle\\Document\\Information',
            'property' => 'name',
        ));
        $builder->add('customUrl', 'nlcollection', array(
            'label'        => 'siteMapSettings.customUrl',
            'type'         => new CustomUrlType(),
            'allow_add'    => true,
            'allow_delete' => true,
            'by_reference' => false,
        ));

        $builder->get('categories')->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) {
            if ((count($event->getData()) == 1) && (array_values($event->getData())[0] === "")) {
                $event->setData(null);
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\StoreBundle\Document\Embedded\SiteMapSetting',
            'translation_domain' => 'NitraStoreBundle',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'site_map_setting';
    }
}