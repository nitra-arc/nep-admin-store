<?php

namespace Nitra\StoreBundle\Form\Type\Store;

use Admingenerated\NitraStoreBundle\Form\BaseStoreType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditType extends BaseEditType
{
    protected $options = array();

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        parent::buildForm($builder, $options);
    }

    /**
     * Get options by field name
     * @param string    $name
     * @param array     $formOptions
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        switch ($name) {
            case 'limits':
                $formOptions['limits'] = $this->options['limits'];
                break;
        }

        return $formOptions;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'limits'                => array(),
            'translation_domain'    => 'NitraStoreBundle',
        ));
    }
}