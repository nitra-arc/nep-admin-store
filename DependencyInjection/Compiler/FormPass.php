<?php

namespace Nitra\StoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FormPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $resources = $container->getParameter('twig.form.resources');
        $resources[] = 'NitraStoreBundle:Form:maskedInput.html.twig';
        $resources[] = 'NitraStoreBundle:Form:productNames.html.twig';

        $container->setParameter('twig.form.resources', $resources);
    }
}