<?php

namespace Nitra\StoreBundle\Document\Embedded;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\EmbeddedDocument
 */
class SiteMapSetting
{
    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var \Nitra\StoreBundle\Document\Embedded\CustomUrl Игнорируемые ссылки
     * @ODM\EmbedMany(targetDocument="Nitra\StoreBundle\Document\Embedded\CustomUrl", strategy="setArray")
     */
    protected $customUrl;

    /**
     * @var \Nitra\ProductBundle\Document\Category[] Игнорируемые категории
     * @ODM\ReferenceMany(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $categories;

    /**
     * @var \Nitra\ProductBundle\Document\Product[] Игнорируемые товары
     * @ODM\ReferenceMany(targetDocument="Nitra\ProductBundle\Document\Product")
     */
    protected $products;

    /**
     * @var \Nitra\InformationBundle\Document\Information[] Игнорируемые статьи
     * @ODM\ReferenceMany(targetDocument="Nitra\InformationBundle\Document\Information")
     */
    protected $informations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customUrl    = new ArrayCollection();
        $this->categories   = new ArrayCollection();
        $this->products     = new ArrayCollection();
        $this->informations = new ArrayCollection();
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add customUrl
     * @param \Nitra\StoreBundle\Document\Embedded\CustomUrl $customUrl
     * @return self
     */
    public function addCustomUrl(CustomUrl $customUrl)
    {
        $this->customUrl[] = $customUrl;
        return $this;
    }

    /**
     * Remove customUrl
     * @param \Nitra\StoreBundle\Document\Embedded\CustomUrl $customUrl
     * @return self
     */
    public function removeCustomUrl(CustomUrl $customUrl)
    {
        $this->customUrl->removeElement($customUrl);
        return $this;
    }

    /**
     * Get customUrl
     * @return \Nitra\StoreBundle\Document\Embedded\CustomUrl[] $customUrl
     */
    public function getCustomUrl()
    {
        return $this->customUrl;
    }

    /**
     * Add category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function addCategory(\Nitra\ProductBundle\Document\Category $category)
    {
        $this->categories[] = $category;
        return $this;
    }

    /**
     * Remove category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function removeCategory(\Nitra\ProductBundle\Document\Category $category)
    {
        $this->categories->removeElement($category);
        return $this;
    }

    /**
     * Get categories
     * @return \Nitra\ProductBundle\Document\Category[] $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set categories
     * @param array $categories
     * @return self
     */
    public function setCategories($categories)
    {
        $this->categories = $categories ? $categories : array();
        return $this;
    }

    /**
     * Add product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return self
     */
    public function addProduct(\Nitra\ProductBundle\Document\Product $product)
    {
        $this->products[] = $product;
        return $this;
    }

    /**
     * Remove product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return self
     */
    public function removeProduct(\Nitra\ProductBundle\Document\Product $product)
    {
        $this->products->removeElement($product);
        return $this;
    }

    /**
     * Get products
     * @return \Nitra\ProductBundle\Document\Product[] $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set products
     * @param \Nitra\ProductBundle\Document\Product[] $products
     * @return self
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * Add information
     * @param \Nitra\InformationBundle\Document\Information $information
     * @return self
     */
    public function addInformation(\Nitra\InformationBundle\Document\Information $information)
    {
        $this->informations[] = $information;
        return $this;
    }

    /**
     * Remove information
     * @param \Nitra\InformationBundle\Document\Information $information
     * @return self
     */
    public function removeInformation(\Nitra\InformationBundle\Document\Information $information)
    {
        $this->informations->removeElement($information);
        return $this;
    }

    /**
     * Get informations
     * @return \Nitra\InformationBundle\Document\Information[] $informations
     */
    public function getInformations()
    {
        return $this->informations;
    }
}