<?php

namespace Nitra\StoreBundle\Document\Embedded;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\EmbeddedDocument
 */
class Email
{
    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string E-mail адрес
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 100)
     */
    protected $email;

    /**
     * @var boolean Видимый
     * @ODM\Boolean
     */
    protected $status;

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Email
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get Email
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     * @param boolean $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     * @return boolean $status
     */
    public function getStatus()
    {
        return $this->status;
    }
}