<?php

namespace Nitra\StoreBundle\Controller\Store;

use Admingenerated\NitraStoreBundle\BaseStoreController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    /**
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\StoreBundle\Document\Store $Store
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\StoreBundle\Document\Store $Store)
    {
        $this->getUser()->addStores($Store);
    }
}