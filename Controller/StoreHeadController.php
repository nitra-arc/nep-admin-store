<?php

namespace Nitra\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class StoreHeadController extends Controller
{
    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->get('doctrine_mongodb.odm.document_manager');
    }
    /**
     * @return \Symfony\Component\Translation\TranslatorInterface
     */
    protected function getTranslator()
    {
        return $this->get('translator');
    }

    /**
     * @Template("NitraStoreBundle::stores.html.twig")
     */
    public function storesAction(Request $request)
    {
        $stores = $this->getUser()->getStores();
        $store  = $request->getSession()->get('store_id');

        return array(
            'stores'       => $stores,
            'active_store' => $store,
        );
    }

    /**
     * @Route("/set-store/{id}", name="set_store")
     */
    public function setStoreAction(Request $request, $id = 0)
    {
        $session = $request->getSession();
        if ($id) {
            foreach ($this->getUser()->getStores() as $uStore) {
                if ($uStore->getId() == $id) {
                    $session->set('store_id', $id);
                }
            }
        }

        return new RedirectResponse($this->generateUrl("root"));
    }

    /**
     * @Route("/configs", name="Nitra_StoreBundle_Store_configs")
     * @Template("NitraStoreBundle::configs.html.twig")
     */
    public function configsAction()
    {
        $router     = $this->container->get('router');
        $collection = $router->getRouteCollection();
        $allRoutes  = $collection->all();

        $routes = array();
        foreach ($allRoutes as $name => $route) {
            if (preg_match("/\/configs\//", $route->getPath())) {
                $slash = 0;
                foreach (explode('/', $route->getPath()) as $path) {
                    if ($path) {
                        $slash++;
                    }
                }
                if ($slash == 3) {
                    $routes[$name] = $this->generateUrl($name);
                }
            }
        }

        return array(
            'routes' => $routes,
        );
    }

    /**
     * @Route("/allowed-values-for-generate-product-names", name="Nitra_StoreBundle_Store_allowedValuesForGenerateProductName")
     */
    public function allowedValuesForGenerateProductName(Request $request)
    {
        $addProducts    = $request->request->get('products');
        $addParameters  = $request->request->get('parameters');
        $parametersType = $request->request->get('parametersType', 'all');
        $categoryId     = $request->request->get('category');

        $result = $this->getDefaultsValuesForGenerateProductName($addProducts);

        $regexp = $request->request->get('term')
            ? "/(?=.*" . preg_replace('/[ ]+/', ')(?=.*', trim($request->request->get('term'))) . ")/i"
            : "/.*/";

        foreach ($result as $key => $value) {
            if (!preg_match($regexp, $value)) {
                unset ($result[$key]);
            }
        }

        if ($addParameters) {
            $qb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Parameter')
                ->hydrate(false)->select('internalName', 'name')
                ->field('name')->equals(new \MongoRegex($regexp));
            if ($categoryId) {
                $category = $this->getDocumentManager()->find('NitraProductBundle:Category', $categoryId);
                $ids = $this->getDocumentManager()->getRepository('NitraProductBundle:Category')
                    ->getParentTreeId($category);

                $qb->field('categories.id')->in($ids);
            }
            if ($parametersType != 'all') {
                $qb->field('type')->equals($parametersType);
            }

            $parameters = $qb->getQuery()->execute();

            $prefix = $this->trans('productManagement.productNameValues.parameter');
            foreach ($parameters as $id => $parameter) {
                $result[$id]  = $prefix . ' | ';
                $result[$id] .= array_key_exists('internalName', $parameter) && $parameter['internalName']
                    ? $parameter['internalName']
                    : $parameter['name'];
            }
        }

        return new JsonResponse($result);
    }

    /**
     * Get defaul values
     * @param boolean $addProducts
     * @return array
     */
    protected function getDefaultsValuesForGenerateProductName($addProducts)
    {
        $transPrefix = 'productManagement.productNameValues.';

        $options = array(
            'category-singularName',
            'category-name',
            'brand-name',
            'model-name',
        );
        if ($addProducts) {
            $options[] = 'product-name';
            $options[] = 'product-article';
        }

        $result = array();
        foreach ($options as $option) {
            $result[$option] = $this->trans($transPrefix . $option);
        }

        return $result;
    }

    /**
     * Translates the given message.
     * @param string $id         The message id (may also be an object that can be cast to string)
     * @param array  $parameters An array of parameters for the message
     * @param string $domain     The domain for the message
     * @return string The translated string
     */
    protected function trans($id, array $parameters = array(), $domain = 'NitraStoreBundle')
    {
        return $this->getTranslator()->trans($id, $parameters, $domain);
    }
}