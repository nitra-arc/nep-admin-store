<?php

namespace Nitra\StoreBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

class KernelRequestListener
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;
    /** @var \Twig_Environment */
    protected $twig;

    /**
     * Constructor
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dm        = $container->get('doctrine_mongodb.odm.document_manager');
        $this->twig      = $container->get('twig');
    }

    /**
     * On kernel request handler
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->dm->getFilterCollection()->has('store')) {
            throw new InvalidConfigurationException('You must configure mongodb odm store filter. Show README.md of NitraStoreBundle');
        }

        $storeId    = $event->getRequest()->getSession()->get('store_id');
        $filter     = $this->dm->getFilterCollection()->getFilter('store');

        $filter->setParameter('storeId', $storeId);

        $this->twig->addGlobal('storeId', $storeId);
    }
}