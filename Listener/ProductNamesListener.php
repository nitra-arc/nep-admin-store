<?php

namespace Nitra\StoreBundle\Listener;

use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;

class ProductNamesListener
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface Service container
     */
    protected $container;

    /**
     * Constructor
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Get current store
     *
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return \Nitra\StoreBundle\Document\Store
     */
    protected function getStore($dm)
    {
        // if php cli
        if (!$this->container->isScopeActive('request')) {
            // return first store
            return $dm->getRepository('NitraStoreBundle:Store')->findOneBy(array());
        }

        // else get store from session
        $storeId = $this->container->get('session')->get('store_id');

        // return found store by id
        return $dm->find('NitraStoreBundle:Store', $storeId);
    }

    /**
     * Get store settings for generate names
     *
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param \Nitra\ProductBundle\Document\Product|\Nitra\ProductBundle\Document\Model $item
     *
     * @return array
     */
    protected function getSettings($dm, $item = null)
    {
        // define result array
        $settings = array(
            'fullProductName'  => array(),
            'shortProductName' => array(),
            'fullModelName'    => array(),
            'shortModelName'   => array(),
        );

        // if item not null
        if ($item) {
            // get category from product or model
            $category = $item instanceof \Nitra\ProductBundle\Document\Product
                ? $item->getModel()->getCategory()
                : $item->getCategory();
            // recursive get category settings
            $settings = $this->getSettingsFromCategory($category, $settings);
        }

        // get store
        $store           = $this->getStore($dm);
        if ($store) {
            // get default settings from store
            $defaultSettings = $store->getProductManagement();

            // fill empty values of category settings
            foreach ($settings as $key => &$value) {
                $value = $value ?: $defaultSettings->{'get' . ucfirst($key)}();
            }
        }

        // return settings
        return $settings;
    }

    /**
     * Recursive get settings from categories
     *
     * @param \Nitra\ProductBundle\Document\Category $category
     * @param array $result
     *
     * @return array
     */
    protected function getSettingsFromCategory($category, array $result)
    {
        // define filled values counter
        $filledAmount = 0;
        // iterate result array
        foreach ($result as $key => &$value) {
            // if not value
            if (!$value) {
                // get value from category
                $value = $category->{'get' . ucfirst($key)}();
            }
            // if value
            if ($value) {
                // increment counter
                $filledAmount ++;
            }
        }

        // if not exists parent category or all values if filled
        return !$category->getParent() || $filledAmount == count($result)
            // return results
            ? $result
            // else - call this function with parent category
            : $this->getSettingsFromCategory($category->getParent(), $result);
    }

    /**
     * On flush doctrine event handler
     *
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $dm         = $args->getDocumentManager();
        $uow        = $dm->getUnitOfWork();

        $updates    = $uow->getScheduledDocumentUpdates();
        $insertions = $uow->getScheduledDocumentInsertions();

        foreach ($updates as $update) {
            $this->handleUpdate($update, $dm);
        }

        foreach ($insertions as $insert) {
            $this->handleInsert($insert, $dm);
        }
    }

    /**
     * Handler for updates objects
     *
     * @param object                                $object
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    protected function handleUpdate($object, $dm)
    {
        $namepsace = explode('\\', get_class($object));
        $method    = 'getProductsAndModelsBy' . end($namepsace);

        if (!method_exists($this, $method)) {
            return;
        }

        $productsAndModels = $this->$method($object, $dm);
        if (!$productsAndModels) {
            return;
        }

        list ($products, $models) = $productsAndModels;


        // get metadata for models
        $modelMeta    = $dm->getRepository('NitraProductBundle:Model')->getClassMetadata();
        // get metadata for products
        $productsMeta = $dm->getRepository('NitraProductBundle:Product')->getClassMetadata();

        // update models
        $this->handleUpdateProductsAndModels(
            $models,
            $dm,
            $modelMeta,
            'Model',
            $object instanceof \Nitra\ProductBundle\Document\Product,
            $object instanceof \Nitra\ProductBundle\Document\Model
        );

        // update products
        $this->handleUpdateProductsAndModels(
            $products,
            $dm,
            $productsMeta,
            'Product',
            $object instanceof \Nitra\ProductBundle\Document\Product,
            $object instanceof \Nitra\ProductBundle\Document\Model
        );
    }

    /**
     * Get products and models by store
     *
     * @param \Nitra\StoreBundle\Document\Store $store
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return array|null
     */
    protected function getProductsAndModelsByStore($store, $dm)
    {
        $changes = $dm->getUnitOfWork()->getDocumentChangeSet($store);
        if (!array_key_exists('productManagement', $changes)) {
            return;
        }

        return array(
            $dm->getRepository('NitraProductBundle:Product')->findAll(),
            $dm->getRepository('NitraProductBundle:Model')->findAll(),
        );
    }

    /**
     * Get products and models by category
     *
     * @param \Nitra\ProductBundle\Document\Category $category
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return array
     */
    protected function getProductsAndModelsByCategory($category, $dm)
    {
        $models    = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->field('category.id')->equals($category->getId())
            ->getQuery()->execute();
        $modelsIds = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('_id')
            ->field('category.id')->equals($category->getId())
            ->getQuery()->execute()->toArray();
        $products  = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->in(array_keys($modelsIds))
            ->getQuery()->execute();

        return array(
            $products,
            $models,
        );
    }

    /**
     * Get products and models by brand
     *
     * @param \Nitra\ProductBundle\Document\Brand $brand
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return array
     */
    protected function getProductsAndModelsByBrand($brand, $dm)
    {
        $models    = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->field('brand.id')->equals($brand->getId())
            ->getQuery()->execute();
        $modelsIds = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('_id')
            ->field('brand.id')->equals($brand->getId())
            ->getQuery()->execute()->toArray();
        $products  = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->in(array_keys($modelsIds))
            ->getQuery()->execute();

        return array(
            $products,
            $models,
        );
    }

    /**
     * Get products and models by parameter
     *
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return array
     */
    protected function getProductsAndModelsByParameter($parameter, $dm)
    {
        $ids = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('model', '_id')
            ->field('parameters.parameter')->equals(new \MongoId($parameter->getId()))
            ->getQuery()->execute()->toArray();
        $modelsIds = array();
        foreach ($ids as $id) {
            $modelsIds[] = $id['model']['$id'];
        }

        $products  = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('id')->in(array_keys($ids))
            ->getQuery()->execute();

        $models    = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->field('_id')->in($modelsIds)
            ->getQuery()->execute();

        return array(
            $products,
            $models,
        );
    }

    /**
     * Get products and models by model
     *
     * @param \Nitra\ProductBundle\Document\Model $model
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return array
     */
    protected function getProductsAndModelsByModel($model, $dm)
    {
        $products = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('model.id')->equals($model->getId())
            ->getQuery()->execute();

        return array(
            $products,
            array($model),
        );
    }

    /**
     * Get products and models by product
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return array
     */
    protected function getProductsAndModelsByProduct($product, $dm)
    {
        return array(
            array($product),
            array($product->getModel()),
        );
    }

    /**
     * Update products
     *
     * @param \Nitra\ProductBundle\Document\Product[]       $items
     * @param \Doctrine\ODM\MongoDB\DocumentManager         $dm
     * @param \Doctrine\ODM\MongoDB\Mapping\ClassMetadata   $meta
     * @param string                                        $document
     * @param boolean                                       $isProduct
     * @param boolean                                       $isModel
     */
    protected function handleUpdateProductsAndModels($items, $dm, $meta, $document, $isProduct, $isModel)
    {
        // get store
        $store = $this->getStore($dm);
        // iterate items
        foreach ($items as $item) {
            // get settings for item by him category
            $settings = $this->getSettings($dm, $item);

            // get full names array
            $storesFullNames  = $item->getFullNames();
            // get short names array
            $storesShortNames = $item->getShortNames();

            // generate new full name
            $newFullName      = $this->generate($item, $settings['full' . $document . 'Name'], $dm);
            // generate new short name
            $newShortName     = $this->generate($item, $settings['short' . $document . 'Name'], $dm);

            // if item has old full name for current store
            if (array_key_exists($store->getId(), $storesFullNames) &&
                // and old full name equal new full name
                ($newFullName == $storesFullNames[$store->getId()]) &&
                // and item has old short name for current store
                array_key_exists($store->getId(), $storesShortNames) &&
                // and old short name equal new short name
                ($newShortName == $storesShortNames[$store->getId()])
            ) {
                // continue - to next item
                continue;
            }

            // set new full item name to array by store id
            $storesFullNames[$store->getId()]  = $newFullName;
            // set new short item name to array by store id
            $storesShortNames[$store->getId()] = $newShortName;

            // set result array with full names to item
            $item->setFullNames($storesFullNames);
            // set result array with short names to item
            $item->setShortNames($storesShortNames);

            // compute changes
            $dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($meta, $item);
        }
    }

    /**
     * Handler for insertions objects
     *
     * @param object                                $object
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    protected function handleInsert($object, $dm)
    {
        if ($object instanceof \Nitra\ProductBundle\Document\Product) {
            $document = 'Product';
        } elseif ($object instanceof \Nitra\ProductBundle\Document\Model) {
            $document = 'Model';
        } else {
            return;
        }

        // get metadata for product
        $meta  = $dm->getRepository('NitraProductBundle:' . $document)->getClassMetadata();
        // get store
        $store = $this->getStore($dm);

        // define full names array
        $storesFullNames  = array();
        // define short names array
        $storesShortNames = array();

        // get settings for generate names
        $settings = $this->getSettings($dm, $object);

        // generate new full name
        $fullName  = $this->generate($object, $settings['full' . $document . 'Name'], $dm);
        // generate new short name
        $shortName = $this->generate($object, $settings['short' . $document . 'Name'], $dm);

        // set new full product name to array by store id
        $storesFullNames[$store->getId()]  = $fullName;
        // set new short product name to array by store id
        $storesShortNames[$store->getId()] = $shortName;

        // compute changes for single document
        $dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($meta, $object);
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param string $field
     * @param string $id
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    protected function getProducts($dm, $field, $id)
    {
        $qb = $dm->createQueryBuilder('NitraProductBundle:Product');

        if (preg_match('/^[0-9a-f]{24}$/', $field['field'])) {
            $qb->field('parameters.parameter')->equals(new \MongoId($id));
        } elseif (preg_match('/^(category|brand)-/', $field['field'])) {
            $field = preg_replace('/^(category|brand)-.*/', '${1}', $field['field']) . '.$id';
            $modelsIds = $dm->createQueryBuilder('NitraProductBundle:Model')
                ->hydrate(false)->select('_id')
                ->field($field)->equals(new \MongoId($id))
                ->getQuery()->execute()->toArray();

            $qb->field('model.id')->in(array_keys($modelsIds));
        } elseif (preg_match('/^model-/', $field['field'])) {
            $qb->field('model.id')->equals($id);
        } else {
            $qb->field('id')->equals($id);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param string $field
     * @param string $id
     *
     * @return \Nitra\ProductBundle\Document\Model[]
     */
    protected function getModels($dm, $field, $id)
    {
        $qb = $dm->createQueryBuilder('NitraProductBundle:Model');

        if (preg_match('/^[0-9a-f]{24}$/', $field['field'])) {
            $modelsIds = $dm->createQueryBuilder('NitraProductBundle:Product')
                ->distinct('model.$id')
                ->field('parameters.parameter')->equals(new \MongoId($id))
                ->getQuery()->execute();
            echo '<pre>' . print_r($modelsIds, true) . '</pre>';
            die (sprintf('in file "%s" on line <b>%s</b>', __FILE__, __LINE__));

            $qb->field('_id')->in($modelsIds);
        } elseif (preg_match('/^(category|brand)-/', $field['field'])) {
            $field = preg_replace('/^(category|brand)-.*/', '${1}', $field['field']) . '.id';
            $qb->field($field)->equals($id);
        } else {
            $qb->field('id')->equals($id);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * Generate new name for product
     *
     * @param \Nitra\ProductBundle\Document\Product|\Nitra\ProductBundle\Document\Model $item
     * @param array $settings
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return string
     */
    protected function generate($item, $settings, $dm)
    {
        $words = array();
        foreach ($settings as $field) {
            if ($word = $this->getValue($field['field'], $item, $dm)) {
                $words[] = $word;
            }
        }

        return trim(implode(' ', $words));
    }

    /**
     * Get value from product
     *
     * @param string $field
     * @param \Nitra\ProductBundle\Document\Product|\Nitra\ProductBundle\Document\Model $item
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     *
     * @return string
     */
    protected function getValue($field, $item, $dm)
    {
        if (preg_match('/^text\|\|\|/', $field)) {
            return preg_replace('/^text\|\|\|/', '', $field);
        }

        $isProduct = $item instanceof \Nitra\ProductBundle\Document\Product;

        if (preg_match('/^[0-9a-f]{24}$/', $field)) {
            $product = $isProduct
                ? $item
                : $dm->createQueryBuilder('NitraProductBundle:Product')
                    ->field('model.id')->equals($item->getId())
                    ->getQuery()->execute()->getSingleResult();

            return $this->getParameterValuesFromProduct($product, $field);
        }

        if (preg_match('/^product-/', $field)) {
            $getter = 'get' . ucfirst(preg_replace('/^product-/', '', $field));
            return $item->$getter();
        }

        if (preg_match('/^model-/', $field)) {
            $getter = 'get' . ucfirst(preg_replace('/^model-/', '', $field));

            $from = $isProduct ? $item->getModel() : $item;

            return $from->$getter();
        }

        $matches = array();
        if (preg_match('/^(brand|category)-(.*)/', $field, $matches)) {
            $from = $isProduct ? $item->getModel() : $item;

            $first  = 'get' . ucfirst($matches[1]);
            $second = 'get' . ucfirst($matches[2]);

            return $from->$first()->$second();
        }
    }

    /**
     * Get all product parameter values as string (comma separated)
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param string $id
     *
     * @return string|null
     */
    protected function getParameterValuesFromProduct($product, $id)
    {
        if (!$product) {
            return;
        }

        foreach ($product->getParameters() as $parameter) {
            if ($parameter->getParameter()->getId() == $id) {
                $values = array();
                foreach ($parameter->getValues() as $value) {
                    $values[] = (string) $value;
                }

                return implode(', ', $values);
            }
        }
    }
}