<?php

namespace Nitra\StoreBundle\Listener;

use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Nitra\SeoBundle\Document\Seo;

/**
 * Listener of doctrine onFlush event for clear site cache (apc)
 */
class CacheListener
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface Container object
     */
    protected $container;

    /**
     * Constructor
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get object for management apc cache
     * @return \Doctrine\Common\Cache\ApcCache
     */
    protected function getCache()
    {
        return $this->container->get('cache_apc');
    }

    /**
     * Get document manager
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->container->get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * Get request object if scope is active
     * @return \Symfony\Component\HttpFoundation\Request
     */
    protected function getRequest()
    {
        return $this->container->isScopeActive('request')
            ? $this->container->get('request')
            : null;
    }

    /**
     * On flush event handler
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $dm      = $args->getDocumentManager();
        $uow     = $dm->getUnitOfWork();
        $updates = $uow->getScheduledDocumentUpdates();
        $inserts = $uow->getScheduledDocumentInsertions();

        foreach ($updates as $entrie) {
            $this->handle($entrie);
        }
        foreach ($inserts as $entrie) {
            $this->handle($entrie, true);
        }
    }

    /**
     * Handle updates and insertions objects
     * @param object    $object
     * @param boolean   $insertions
     */
    protected function handle($object, $insertions = false)
    {
        $class = get_class($object);
        list (, $bundle, , $document) = explode('\\', $class);

        $method = 'clear' . $bundle . $document;

        if (method_exists($this, $method)) {
            $this->$method($object);

            $this->clearSeo($object, $insertions);
        }
    }

    /**
     * Map cache key - wrap key with database name and store
     * @param string $key
     * @return string
     */
    protected function mapCacheKey($key)
    {
        $mdb    = $this->container->getParameter('mongo_database_name');
        $sId    = $this->getRequest() ? $this->getRequest()->getSession()->get('store_id') : null;
        $store  = $this->getDocumentManager()->find('NitraStoreBundle:Store', $sId);
        $sHost  = $store ? $store->getHost() : null;

        return $mdb . '_' . $key . '_' . $sHost;
    }

    /**
     * Clear cache by seo
     * @param object $obj
     * @param boolean $insert
     */
    protected function clearSeo($obj, $insert = false)
    {
        $key        = $this->mapCacheKey('seo_info_by_uris');

        $reflaction = new \ReflectionClass($obj);
        $refKey     = $reflaction->getShortName() . (!$insert ? ('_' . $obj->getId()) : '' );
        $seos       = $this->getCache()->contains($key) ? $this->getCache()->fetch($key) : array();

        foreach ($seos as $uri => $seo) {
            if ((($obj instanceof Seo) && ($obj->getKey() == $uri)) || (in_array($refKey, $seo['source']))) {
                unset($seos[$uri]);
            }
        }

        $this->getCache()->save($key, $seos, 60*60);
    }

    /**
     * Clear cache by store
     * @param \Nitra\StoreBundle\Document\Store $store
     */
    protected function clearStoreBundleStore($store)
    {
        $mdb    = $this->container->getParameter('mongo_database_name');
        $this->getCache()->delete($mdb . "_store_" . $store->getHost());
    }

    /**
     * Clear cache by product
     * @param \Nitra\ProductBundle\Document\Product $product
     */
    protected function clearProductBundleProduct($product)
    {
        $cache  = $this->getCache();

        $keys   = array();
        $keys[] = $this->mapCacheKey('nitra_nice_product_urls');

        foreach ($keys as $key) {
            $cache->delete($key);
        }
    }

    /**
     * Clear cache by category
     */
    protected function clearProductBundleCategory()
    {
        $cache  = $this->getCache();

        $cache->delete($this->mapCacheKey('nitra_nice_category_urls'));
        $cache->delete($this->mapCacheKey('nitra_nice_product_urls'));
    }

    /**
     * Clear cache by brand
     */
    protected function clearProductBundleBrand()
    {
        $cache  = $this->getCache();

        $cache->delete($this->mapCacheKey('nitra_nice_brand_urls'));
    }

    /**
     * Clear cache of review settings
     */
    protected function clearReviewBundleReviewSettings()
    {
        $key   = $this->mapCacheKey('show_unmoderated');
        $this->getCache()->delete($key);
    }
}